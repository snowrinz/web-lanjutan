var express = require('express');
var app = express();
var bodyParser = require("body-parser");

app.get("/restaurant/find", (req, res, next) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  var name = req.query.name;
  res.send(`restaurant ${name} found !`)
});


app.get("/restaurant/:id/", (req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  var id = req.params.id
  res.send(`restaurant with id ${id} has been found with name Makan`);
});


app.listen(4000,  () => {
  console.log("Server run");
});
