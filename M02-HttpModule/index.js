var http = require("http");

// http options
var options = {
  hostname: "www.w3schools.com",
  port: 80,
  path: "/",
  method: "GET",
  
  headers: {
    "Content-Type": "application/json",
  },
};

var req = http.request(options, (res) => {
  console.log(res.statusCode);
  console.log(res.statusMessage);
  console.log(res.headers);
});

// handle error
req.on("error", function (e) {
  console.log("Error: " + e.message);
});

// close connection
req.end();
