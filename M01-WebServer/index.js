const http = require('http');

// server with port 3000
http.createServer((req, res)=>{
  // http header
  res.writeHead(200, {'Content-Type': 'text/html'})
  
  // send respond to client
  res.write("Module HTTP Mobile dan Web")

  // finish respond
  res.end();
}).listen(3000); // port 3000