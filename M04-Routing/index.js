var express = require('express');
var server = express();
var bodyParser = require("body-parser");

server.use(express.static(__dirname + "/public"));

server.get("/restaurant/find", (req, res, next) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  var name = req.query.name;
  res.send(`restaurant ${name} found !`)
});

// GET method route
server.get('/easter-egg', (req, res) => {
  res.send('Easter Egg Page! Look for more clue');
})

// POST method route
server.post('/', (req, res) => {
  res.send('access using POST request')
})

var data = bodyParser.urlencoded({ extended: false });
server.post("/restaurant/register", data, (req, res) => {
  console.log(req);
  res.send(`restaurant has been registered with ${req.body}`);
});


server.listen(4000, () => {
  console.log('Server Run');
})

