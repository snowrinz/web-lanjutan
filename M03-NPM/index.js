var express = require('express');
var logger = require('morgan');

var server = express();

server.use(logger('dev'));

server.use(express.static(__dirname + '/public'));

server.listen(4000, () => {
  console.log('Server Run');
})

