var express = require('express');
var app = express();
var bodyParser = require("body-parser");

//Middleware cek id
const myMiddleware = (req, res, next) => {
  if (req.params.id > 0) {
    console.log("id terverifikasi");
    next();
  } else {
    const err = {
      status: "error",
      data: {
        id: req.params.id,
      },
    };
    next(err);
  }
};

app.get("/restaurant/find", (req, res, next) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  var name = req.query.name;
  res.send(`restaurant ${name} found !`)
});


app.get("/restaurant/:id/", myMiddleware, (req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/plain");
  var id = req.params.id
  res.send(`restaurant with id ${id} has been found with name Makan`);
});

//ErrorHandling
app.use((error, req, res, next) => {
  res.send(error);
});


app.listen(4000,  () => {
  console.log("Server run");
});
